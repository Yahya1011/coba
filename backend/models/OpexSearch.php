<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Opex;

/**
 * OpexSearch represents the model behind the search form of `backend\models\Opex`.
 */
class OpexSearch extends Opex
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_opex', 'cat_coa_id', 'subcat_coa_id', 'nominal', 'pic_id'], 'integer'],
            [['code_trx', 'date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Opex::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_opex' => $this->id_opex,
            'date' => $this->date,
            'cat_coa_id' => $this->cat_coa_id,
            'subcat_coa_id' => $this->subcat_coa_id,
            'nominal' => $this->nominal,
            'pic_id' => $this->pic_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code_trx', $this->code_trx]);

        return $dataProvider;
    }
}
