<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subdistrict".
 *
 * @property int $id_subdistrict
 * @property int $city_district_id
 * @property string $name
 *
 * @property CityDistrict $cityDistrict
 * @property UserProfile[] $userProfiles
 * @property Village[] $villages
 */
class Subdistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subdistrict';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_district_id', 'name'], 'required'],
            [['city_district_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['city_district_id'], 'exist', 'skipOnError' => true, 'targetClass' => CityDistrict::className(), 'targetAttribute' => ['city_district_id' => 'id_city_district']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_subdistrict' => 'Id Subdistrict',
            'city_district_id' => 'City District Name',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityDistrict()
    {
        return $this->hasOne(CityDistrict::className(), ['id_city_district' => 'city_district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['subdistricts_id' => 'id_subdistrict']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVillages()
    {
        return $this->hasMany(Village::className(), ['subdistricts_id' => 'id_subdistrict']);
    }
}
