<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DonationOffline;

/**
 * DonationOfflineSearch represents the model behind the search form of `backend\models\DonationOffline`.
 */
class DonationOfflineSearch extends DonationOffline
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_donation_offline', 'cat_donation_id', 'nominal'], 'integer'],
            [['code_trx', 'date', 'username', 'info', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DonationOffline::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_donation_offline' => $this->id_donation_offline,
            'date' => $this->date,
            'cat_donation_id' => $this->cat_donation_id,
            'nominal' => $this->nominal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code_trx', $this->code_trx])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'info', $this->info]);

        return $dataProvider;
    }
}
