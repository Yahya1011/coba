<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "donation_online".
 *
 * @property int $id_donation_online
 * @property string $code_trx
 * @property string $date
 * @property int $bank_id
 * @property string $no_rek
 * @property string $account_name
 * @property int $nominal
 * @property string $info
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Bank $bank
 */
class DonationOnline extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'donation_online';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_trx', 'date', 'bank_id', 'no_rek', 'account_name', 'nominal'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['bank_id', 'nominal'], 'integer'],
            [['code_trx'], 'string', 'max' => 10],
            [['no_rek'], 'string', 'max' => 20],
            [['account_name'], 'string', 'max' => 100],
            [['info'], 'string', 'max' => 45],
            [['code_trx'], 'unique'],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::className(), 'targetAttribute' => ['bank_id' => 'id_bank']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_donation_online' => 'Id Donation Online',
            'code_trx' => 'Code Trx',
            'date' => 'Date',
            'bank_id' => 'Bank ID',
            'no_rek' => 'No Rek',
            'account_name' => 'Account Name',
            'nominal' => 'Nominal',
            'info' => 'Info',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['id_bank' => 'bank_id']);
    }
}
