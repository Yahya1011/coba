<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ZipCode;

/**
 * ZipCodeSearch represents the model behind the search form of `backend\models\ZipCode`.
 */
class ZipCodeSearch extends ZipCode
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_zip_code', 'village_id', 'no_zip_code'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ZipCode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_zip_code' => $this->id_zip_code,
            'village_id' => $this->village_id,
            'no_zip_code' => $this->no_zip_code,
        ]);

        return $dataProvider;
    }
}
