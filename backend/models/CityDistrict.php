<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "city_district".
 *
 * @property int $id_city_district
 * @property int $counrty_id
 * @property string $name
 *
 * @property Country $counrty
 * @property Subdistrict[] $subdistricts
 * @property UserProfile[] $userProfiles
 */
class CityDistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city_district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['counrty_id', 'name'], 'required'],
            [['counrty_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['counrty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['counrty_id' => 'id_country']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_city_district' => 'Id City District',
            'counrty_id' => 'Country Name',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounrty()
    {
        return $this->hasOne(Country::className(), ['id_country' => 'counrty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistricts()
    {
        return $this->hasMany(Subdistrict::className(), ['city_district_id' => 'id_city_district']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['city_district_id' => 'id_city_district']);
    }
}
