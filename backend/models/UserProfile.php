<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $id_user_profile
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $gender
 * @property string $address
 * @property int $counrty_id
 * @property int $city_district_id
 * @property int $subdistricts_id
 * @property int $village_id
 * @property int $zip_code_id
 * @property string $telp
 * @property string $email
 * @property string $no_rek
 * @property string $photo
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CityDistrict $cityDistrict
 * @property Country $counrty
 * @property Subdistrict $subdistricts
 * @property User $user
 * @property Village $village
 * @property ZipCode $zipCode
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name', 'date_of_birth', 'gender', 'address', 'counrty_id', 'city_district_id', 'subdistricts_id', 'village_id', 'zip_code_id', 'telp', 'email', 'no_rek'], 'required'],
            [['user_id', 'counrty_id', 'city_district_id', 'subdistricts_id', 'village_id', 'zip_code_id'], 'integer'],
            [['date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['gender'], 'string'],
            [['first_name', 'last_name', 'email', 'no_rek'], 'string', 'max' => 45],
            [['address', 'photo'], 'string', 'max' => 255],
            [['telp'], 'string', 'max' => 15],
            [['city_district_id'], 'exist', 'skipOnError' => true, 'targetClass' => CityDistrict::className(), 'targetAttribute' => ['city_district_id' => 'id_city_district']],
            [['counrty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['counrty_id' => 'id_country']],
            [['subdistricts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdistrict::className(), 'targetAttribute' => ['subdistricts_id' => 'id_subdistrict']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['village_id'], 'exist', 'skipOnError' => true, 'targetClass' => Village::className(), 'targetAttribute' => ['village_id' => 'id_village']],
            [['zip_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => ZipCode::className(), 'targetAttribute' => ['zip_code_id' => 'id_zip_code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user_profile' => 'Id User Profile',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'address' => 'Address',
            'counrty_id' => 'Counrty ID',
            'city_district_id' => 'City District ID',
            'subdistricts_id' => 'Subdistricts ID',
            'village_id' => 'Village ID',
            'zip_code_id' => 'Zip Code ID',
            'telp' => 'Telp',
            'email' => 'Email',
            'no_rek' => 'No Rek',
            'photo' => 'Photo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityDistrict()
    {
        return $this->hasOne(CityDistrict::className(), ['id_city_district' => 'city_district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounrty()
    {
        return $this->hasOne(Country::className(), ['id_country' => 'counrty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistricts()
    {
        return $this->hasOne(Subdistrict::className(), ['id_subdistrict' => 'subdistricts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVillage()
    {
        return $this->hasOne(Village::className(), ['id_village' => 'village_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZipCode()
    {
        return $this->hasOne(ZipCode::className(), ['id_zip_code' => 'zip_code_id']);
    }
}
