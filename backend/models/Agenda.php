<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "agenda".
 *
 * @property int $id_agenda
 * @property int $pic_id_pic
 * @property string $date
 * @property int $event_id
 * @property string $description
 *
 * @property Event $event
 * @property Pic $picIdPic
 */
class Agenda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agenda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pic_id_pic', 'date', 'event_id'], 'required'],
            [['pic_id_pic', 'event_id'], 'integer'],
            [['date'], 'safe'],
            [['description'], 'string', 'max' => 225],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id_event']],
            [['pic_id_pic'], 'exist', 'skipOnError' => true, 'targetClass' => Pic::className(), 'targetAttribute' => ['pic_id_pic' => 'id_pic']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_agenda' => 'Id Agenda',
            'pic_id_pic' => 'Pic Id Pic',
            'date' => 'Date',
            'event_id' => 'Event ID',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id_event' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPicIdPic()
    {
        return $this->hasOne(Pic::className(), ['id_pic' => 'pic_id_pic']);
    }
}
