<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subcat_coa".
 *
 * @property int $id_subcat_coa
 * @property int $cat_coa_id
 * @property string $code_subcat
 * @property string $name
 *
 * @property Opex[] $opexes
 * @property CatCoa $catCoa
 */
class SubcatCoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subcat_coa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_coa_id', 'code_subcat', 'name'], 'required'],
            [['cat_coa_id'], 'integer'],
            [['code_subcat'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 100],
            [['code_subcat'], 'unique'],
            [['cat_coa_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatCoa::className(), 'targetAttribute' => ['cat_coa_id' => 'id_cat_coa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_subcat_coa' => 'Id Subcat Coa',
            'cat_coa_id' => 'Cat Coa ID',
            'code_subcat' => 'Code Subcat',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpexes()
    {
        return $this->hasMany(Opex::className(), ['subcat_coa_id' => 'id_subcat_coa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatCoa()
    {
        return $this->hasOne(CatCoa::className(), ['id_cat_coa' => 'cat_coa_id']);
    }
}
