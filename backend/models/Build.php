<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "build".
 *
 * @property int $id_build
 * @property string $date
 * @property int $cat_build_id
 * @property int $progress
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CatBuild $catBuild
 */
class Build extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'build';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'cat_build_id', 'progress'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['cat_build_id', 'progress'], 'integer'],
            [['cat_build_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatBuild::className(), 'targetAttribute' => ['cat_build_id' => 'id_cat_build']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_build' => 'Id Build',
            'date' => 'Date',
            'cat_build_id' => 'Cat Build ID',
            'progress' => 'Progress',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatBuild()
    {
        return $this->hasOne(CatBuild::className(), ['id_cat_build' => 'cat_build_id']);
    }
}
