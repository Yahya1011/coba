<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_donation".
 *
 * @property int $id_cat_donation
 * @property string $code_donation
 * @property string $name
 *
 * @property DonationOffline[] $donationOfflines
 */
class CatDonation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_donation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_donation', 'name'], 'required'],
            [['code_donation'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 100],
            [['code_donation'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cat_donation' => 'Id Cat Donation',
            'code_donation' => 'Code Donation',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonationOfflines()
    {
        return $this->hasMany(DonationOffline::className(), ['cat_donation_id' => 'id_cat_donation']);
    }
}
