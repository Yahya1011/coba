<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\UserProfile;

/**
 * UserProfileSearch represents the model behind the search form of `backend\models\UserProfile`.
 */
class UserProfileSearch extends UserProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user_profile', 'user_id', 'counrty_id', 'city_district_id', 'subdistricts_id', 'village_id', 'zip_code_id'], 'integer'],
            [['first_name', 'last_name', 'date_of_birth', 'gender', 'address', 'telp', 'email', 'no_rek', 'photo', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user_profile' => $this->id_user_profile,
            'user_id' => $this->user_id,
            'date_of_birth' => $this->date_of_birth,
            'counrty_id' => $this->counrty_id,
            'city_district_id' => $this->city_district_id,
            'subdistricts_id' => $this->subdistricts_id,
            'village_id' => $this->village_id,
            'zip_code_id' => $this->zip_code_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'telp', $this->telp])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'no_rek', $this->no_rek])
            ->andFilterWhere(['like', 'photo', $this->photo]);

        return $dataProvider;
    }
}
