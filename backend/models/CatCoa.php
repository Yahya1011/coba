<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_coa".
 *
 * @property int $id_cat_coa
 * @property string $code_coa
 * @property string $name
 *
 * @property Opex[] $opexes
 * @property SubcatCoa[] $subcatCoas
 */
class CatCoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_coa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_coa', 'name'], 'required'],
            [['code_coa'], 'string', 'max' => 10],
            [['name'], 'string', 'max' => 100],
            [['code_coa'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cat_coa' => 'Id Cat Coa',
            'code_coa' => 'Code Coa',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpexes()
    {
        return $this->hasMany(Opex::className(), ['cat_coa_id' => 'id_cat_coa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcatCoas()
    {
        return $this->hasMany(SubcatCoa::className(), ['cat_coa_id' => 'id_cat_coa']);
    }
}
