<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CatDonation;

/**
 * CatDonationSearch represents the model behind the search form of `backend\models\CatDonation`.
 */
class CatDonationSearch extends CatDonation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cat_donation'], 'integer'],
            [['code_donation', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatDonation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_cat_donation' => $this->id_cat_donation,
        ]);

        $query->andFilterWhere(['like', 'code_donation', $this->code_donation])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
