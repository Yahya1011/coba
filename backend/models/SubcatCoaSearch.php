<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SubcatCoa;

/**
 * SubcatCoaSearch represents the model behind the search form of `backend\models\SubcatCoa`.
 */
class SubcatCoaSearch extends SubcatCoa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_subcat_coa', 'cat_coa_id'], 'integer'],
            [['code_subcat', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubcatCoa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_subcat_coa' => $this->id_subcat_coa,
            'cat_coa_id' => $this->cat_coa_id,
        ]);

        $query->andFilterWhere(['like', 'code_subcat', $this->code_subcat])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
