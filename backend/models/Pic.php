<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pic".
 *
 * @property int $id_pic
 * @property string $name
 * @property string $address
 * @property string $telp
 * @property string $email
 * @property string $photo
 *
 * @property Agenda[] $agendas
 * @property Opex[] $opexes
 */
class Pic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address', 'telp', 'email'], 'required'],
            [['name', 'address', 'email', 'photo'], 'string', 'max' => 45],
            [['telp'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pic' => 'Id Pic',
            'name' => 'Name',
            'address' => 'Address',
            'telp' => 'Telp',
            'email' => 'Email',
            'photo' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendas()
    {
        return $this->hasMany(Agenda::className(), ['pic_id_pic' => 'id_pic']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOpexes()
    {
        return $this->hasMany(Opex::className(), ['pic_id' => 'id_pic']);
    }
}
