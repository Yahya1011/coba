<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property int $id_history
 * @property string $code_history
 * @property string $info
 * @property string $date
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['code_history', 'info'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_history' => 'Id History',
            'code_history' => 'Code History',
            'info' => 'Info',
            'date' => 'Date',
        ];
    }
}
