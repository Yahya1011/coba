<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TrxBank;

/**
 * TrxBankSearch represents the model behind the search form of `backend\models\TrxBank`.
 */
class TrxBankSearch extends TrxBank
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_trx_bank', 'bank_id', 'nominal'], 'integer'],
            [['code_trx', 'no_rek_destination', 'account_name_destination', 'info', 'type_trx', 'no_machine', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrxBank::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_trx_bank' => $this->id_trx_bank,
            'bank_id' => $this->bank_id,
            'nominal' => $this->nominal,
        ]);

        $query->andFilterWhere(['like', 'code_trx', $this->code_trx])
            ->andFilterWhere(['like', 'no_rek_destination', $this->no_rek_destination])
            ->andFilterWhere(['like', 'account_name_destination', $this->account_name_destination])
            ->andFilterWhere(['like', 'info', $this->info])
            ->andFilterWhere(['like', 'type_trx', $this->type_trx])
            ->andFilterWhere(['like', 'no_machine', $this->no_machine])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
