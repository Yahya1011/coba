<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "donation_offline".
 *
 * @property int $id_donation_offline
 * @property string $code_trx
 * @property string $date
 * @property int $cat_donation_id
 * @property string $username
 * @property int $nominal
 * @property string $info
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CatDonation $catDonation
 */
class DonationOffline extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'donation_offline';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_trx', 'date', 'cat_donation_id', 'username', 'nominal'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['cat_donation_id', 'nominal'], 'integer'],
            [['code_trx'], 'string', 'max' => 10],
            [['username'], 'string', 'max' => 100],
            [['info'], 'string', 'max' => 45],
            [['code_trx'], 'unique'],
            [['cat_donation_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatDonation::className(), 'targetAttribute' => ['cat_donation_id' => 'id_cat_donation']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_donation_offline' => 'Id Donation Offline',
            'code_trx' => 'Code Trx',
            'date' => 'Date',
            'cat_donation_id' => 'Cat Donation ID',
            'username' => 'Username',
            'nominal' => 'Nominal',
            'info' => 'Info',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatDonation()
    {
        return $this->hasOne(CatDonation::className(), ['id_cat_donation' => 'cat_donation_id']);
    }
}
