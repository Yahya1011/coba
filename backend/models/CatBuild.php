<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_build".
 *
 * @property int $id_cat_build
 * @property string $name
 * @property string $bobot
 *
 * @property Build[] $builds
 */
class CatBuild extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_build';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'bobot'], 'required'],
            [['name', 'bobot'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cat_build' => 'Id Cat Build',
            'name' => 'Name',
            'bobot' => 'Bobot',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilds()
    {
        return $this->hasMany(Build::className(), ['cat_build_id' => 'id_cat_build']);
    }
}
