<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "zip_code".
 *
 * @property int $id_zip_code
 * @property int $village_id
 * @property int $no_zip_code
 *
 * @property UserProfile[] $userProfiles
 * @property Village $village
 */
class ZipCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zip_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['village_id', 'no_zip_code'], 'required'],
            [['village_id', 'no_zip_code'], 'integer'],
            [['no_zip_code'], 'unique'],
            [['village_id'], 'exist', 'skipOnError' => true, 'targetClass' => Village::className(), 'targetAttribute' => ['village_id' => 'id_village']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_zip_code' => 'Id Zip Code',
            'village_id' => 'Village ID',
            'no_zip_code' => 'No Zip Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['zip_code_id' => 'id_zip_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVillage()
    {
        return $this->hasOne(Village::className(), ['id_village' => 'village_id']);
    }
}
