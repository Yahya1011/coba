<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bank".
 *
 * @property int $id_bank
 * @property string $code_bank
 * @property string $name
 *
 * @property DonationOnline[] $donationOnlines
 * @property TrxBank[] $trxBanks
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_bank', 'name'], 'required'],
            [['code_bank', 'name'], 'string', 'max' => 45],
            [['code_bank'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_bank' => 'Id Bank',
            'code_bank' => 'Code Bank',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonationOnlines()
    {
        return $this->hasMany(DonationOnline::className(), ['bank_id' => 'id_bank']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrxBanks()
    {
        return $this->hasMany(TrxBank::className(), ['bank_id' => 'id_bank']);
    }
}
