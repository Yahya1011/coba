<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "opex".
 *
 * @property int $id_opex
 * @property string $code_trx
 * @property string $date
 * @property int $cat_coa_id
 * @property int $subcat_coa_id
 * @property int $nominal
 * @property int $pic_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CatCoa $catCoa
 * @property Pic $pic
 * @property SubcatCoa $subcatCoa
 */
class Opex extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'opex';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_trx', 'date', 'cat_coa_id', 'subcat_coa_id', 'nominal', 'pic_id'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['cat_coa_id', 'subcat_coa_id', 'nominal', 'pic_id'], 'integer'],
            [['code_trx'], 'string', 'max' => 45],
            [['code_trx'], 'unique'],
            [['cat_coa_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatCoa::className(), 'targetAttribute' => ['cat_coa_id' => 'id_cat_coa']],
            [['pic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pic::className(), 'targetAttribute' => ['pic_id' => 'id_pic']],
            [['subcat_coa_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubcatCoa::className(), 'targetAttribute' => ['subcat_coa_id' => 'id_subcat_coa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_opex' => 'Id Opex',
            'code_trx' => 'Code Trx',
            'date' => 'Date',
            'cat_coa_id' => 'Cat Coa ID',
            'subcat_coa_id' => 'Subcat Coa ID',
            'nominal' => 'Nominal',
            'pic_id' => 'Pic ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatCoa()
    {
        return $this->hasOne(CatCoa::className(), ['id_cat_coa' => 'cat_coa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPic()
    {
        return $this->hasOne(Pic::className(), ['id_pic' => 'pic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcatCoa()
    {
        return $this->hasOne(SubcatCoa::className(), ['id_subcat_coa' => 'subcat_coa_id']);
    }
}
