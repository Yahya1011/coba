<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CatCoa;

/**
 * CatCoaSearch represents the model behind the search form of `backend\models\CatCoa`.
 */
class CatCoaSearch extends CatCoa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_cat_coa'], 'integer'],
            [['code_coa', 'name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatCoa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_cat_coa' => $this->id_cat_coa,
        ]);

        $query->andFilterWhere(['like', 'code_coa', $this->code_coa])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
