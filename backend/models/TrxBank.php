<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "trx_bank".
 *
 * @property int $id_trx_bank
 * @property string $code_trx
 * @property int $bank_id
 * @property string $no_rek_destination
 * @property string $account_name_destination
 * @property int $nominal
 * @property string $info
 * @property string $type_trx
 * @property string $no_machine
 * @property string $status
 *
 * @property Bank $bank
 */
class TrxBank extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trx_bank';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_trx', 'bank_id'], 'required'],
            [['bank_id', 'nominal'], 'integer'],
            [['type_trx', 'status'], 'string'],
            [['code_trx', 'no_rek_destination', 'account_name_destination', 'info', 'no_machine'], 'string', 'max' => 45],
            [['code_trx'], 'unique'],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::className(), 'targetAttribute' => ['bank_id' => 'id_bank']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_trx_bank' => 'Id Trx Bank',
            'code_trx' => 'Code Trx',
            'bank_id' => 'Bank ID',
            'no_rek_destination' => 'No Rek Destination',
            'account_name_destination' => 'Account Name Destination',
            'nominal' => 'Nominal',
            'info' => 'Info',
            'type_trx' => 'Type Trx',
            'no_machine' => 'No Machine',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['id_bank' => 'bank_id']);
    }
}
