<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "village".
 *
 * @property int $id_village
 * @property int $subdistricts_id
 * @property string $name
 *
 * @property UserProfile[] $userProfiles
 * @property Subdistrict $subdistricts
 * @property ZipCode[] $zipCodes
 */
class Village extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'village';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdistricts_id', 'name'], 'required'],
            [['subdistricts_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['subdistricts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdistrict::className(), 'targetAttribute' => ['subdistricts_id' => 'id_subdistrict']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_village' => 'Id Village',
            'subdistricts_id' => 'Subdistricts ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfiles()
    {
        return $this->hasMany(UserProfile::className(), ['village_id' => 'id_village']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdistricts()
    {
        return $this->hasOne(Subdistrict::className(), ['id_subdistrict' => 'subdistricts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZipCodes()
    {
        return $this->hasMany(ZipCode::className(), ['village_id' => 'id_village']);
    }
}
