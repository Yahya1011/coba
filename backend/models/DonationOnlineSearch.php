<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DonationOnline;

/**
 * DonationOnlineSearch represents the model behind the search form of `backend\models\DonationOnline`.
 */
class DonationOnlineSearch extends DonationOnline
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_donation_online', 'bank_id', 'nominal'], 'integer'],
            [['code_trx', 'date', 'no_rek', 'account_name', 'info', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DonationOnline::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_donation_online' => $this->id_donation_online,
            'date' => $this->date,
            'bank_id' => $this->bank_id,
            'nominal' => $this->nominal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'code_trx', $this->code_trx])
            ->andFilterWhere(['like', 'no_rek', $this->no_rek])
            ->andFilterWhere(['like', 'account_name', $this->account_name])
            ->andFilterWhere(['like', 'info', $this->info]);

        return $dataProvider;
    }
}
