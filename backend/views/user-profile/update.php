<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UserProfile */

$this->title = 'Update User Profile: ' . $model->id_user_profile;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user_profile, 'url' => ['view', 'id' => $model->id_user_profile]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
