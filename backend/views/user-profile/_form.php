<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\User;
use backend\models\Country;
use backend\models\CityDistrict;
use backend\models\Subdistrict;
use backend\models\Village;
use backend\models\ZipCode;

/* @var $this yii\web\View */
/* @var $model backend\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList(
        ArrayHelper::map(User::find()->all(), 'id', 'username'),
        ['prompt'=>'Select User']
    ) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_of_birth')->textInput() ?>

    <?= $form->field($model, 'gender')->dropDownList([ 'Laki-Laki' => 'Laki-Laki', 'Perempuan' => 'Perempuan', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'counrty_id')->dropDownList(
        ArrayHelper::map(Country::find()->all(), 'id_country', 'name'),
        ['prompt'=>'Select Country']
    ) ?>

    <?= $form->field($model, 'city_district_id')->dropDownList(
        ArrayHelper::map(CityDistrict::find()->all(), 'id_city_district', 'name'),
        ['prompt'=>'Select City District']
    ) ?>

    <?= $form->field($model, 'subdistricts_id')->dropDownList(
        ArrayHelper::map(Subdistrict::find()->all(), 'id_subdistrict', 'name'),
        ['prompt'=>'Select Subdistrict']
    ) ?>

    <?= $form->field($model, 'village_id')->dropDownList(
        ArrayHelper::map(Village::find()->all(), 'id_village', 'name'),
        ['prompt'=>'Select Village']
    ) ?>

    <?= $form->field($model, 'zip_code_id')->dropDownList(
        ArrayHelper::map(ZipCode::find()->all(), 'id_zip_code', 'no_zip_code'),
        ['prompt'=>'Select Zip Code']
    ) ?>

    <?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_rek')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
