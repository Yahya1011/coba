<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SubcatCoa */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcat Coas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcat-coa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_subcat_coa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_subcat_coa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_subcat_coa',
            'cat_coa_id',
            'code_subcat',
            'name',
        ],
    ]) ?>

</div>
