<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SubcatCoa */

$this->title = 'Update Subcat Coa: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcat Coas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_subcat_coa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subcat-coa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
