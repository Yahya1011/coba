<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CatCoa;

/* @var $this yii\web\View */
/* @var $model backend\models\SubcatCoa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subcat-coa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cat_coa_id')->dropDownList(
    	ArrayHelper::map(CatCoa::find()->all(), 'cat_coa_id', 'name'),
    	['prompt'=>'Select Cat Coa']
    ) ?>

    <?= $form->field($model, 'code_subcat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
