<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SubcatCoa */

$this->title = 'Create Subcat Coa';
$this->params['breadcrumbs'][] = ['label' => 'Subcat Coas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcat-coa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
