<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ZipCode */

$this->title = 'Update Zip Code: ' . $model->id_zip_code;
$this->params['breadcrumbs'][] = ['label' => 'Zip Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_zip_code, 'url' => ['view', 'id' => $model->id_zip_code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="zip-code-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
