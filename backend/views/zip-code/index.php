<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ZipCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zip Code';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zip-code-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Zip Code', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_zip_code',
            'village_id',
            'no_zip_code',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
