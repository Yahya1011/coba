<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Village;

/* @var $this yii\web\View */
/* @var $model backend\models\ZipCode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zip-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'village_id')->dropDownList(
    	ArrayHelper::map(Village::find()->all(), 'id_village','name'),
    	['prompt'=>'Select Village']
    ) ?>

    <?= $form->field($model, 'no_zip_code')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
