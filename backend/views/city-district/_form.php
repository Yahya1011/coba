<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Country;

/* @var $this yii\web\View */
/* @var $model backend\models\CityDistrict */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-district-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'counrty_id')->dropDownList(
    	ArrayHelper::map(Country::find()->all(), 'id_country','name'),
    	['prompt'=>'select country']
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
