<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CityDistrict */

$this->title = 'Create City District';
$this->params['breadcrumbs'][] = ['label' => 'City Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-district-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
