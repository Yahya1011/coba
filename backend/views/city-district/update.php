<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CityDistrict */

$this->title = 'Update City District: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'City Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_city_district]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="city-district-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
