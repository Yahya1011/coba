<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Management', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Master Data',
                        'icon' => 'database',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Region',
                                'icon' => 'map',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Country', 'icon' => 'circle-o', 'url' => ['country/index'],],
                                    ['label' => 'City/District', 'icon' => 'circle-o', 'url' => ['city-district/index'],],
                                    ['label' => 'Subdistrict', 'icon' => 'circle-o', 'url' => ['subdistrict/index'],],
                                    ['label' => 'Village', 'icon' => 'circle-o', 'url' => ['village/index'],],
                                    ['label' => 'Zip Code', 'icon' => 'circle-o', 'url' => ['zip-code/index'],],
                                ],
                            ],
                            [
                                'label' => 'COA',
                                'icon' => 'key',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Category COA', 'icon' => 'circle-o', 'url' => ['cat-coa/index'],],
                                    ['label' => 'Subcategory COA', 'icon' => 'circle-o', 'url' => ['subcat-coa/index'],],
                                ],
                            ],
                            [
                                'label' => 'Buiding',
                                'icon' => 'building',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Categori Building', 'icon' => 'circle-o', 'url' => ['cat-build/index'],],
                                ],
                            ],
                            [
                                'label' => 'Bank',
                                'icon' => 'bank ',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Data Bank', 'icon' => 'circle-o', 'url' => ['bank/index'],],
                                    ['label' => 'Transaction Bank', 'icon' => 'circle-o', 'url' => ['trx-bank/index'],],
                                ],
                            ],
                            [
                                'label' => 'Donation',
                                'icon' => 'money',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Category Donation', 'icon' => 'circle-o', 'url' => ['cat-donation/index'],],
                                ],
                            ],
                            ['label' => 'Event', 'icon' => 'calendar-plus-o', 'url' => ['event/index'],],
                            ['label' => 'PIC', 'icon' => 'user', 'url' => ['pic/index'],],

                        ],
                    ],
                    [
                        'label' => 'Management User',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                                    ['label' => 'User', 'icon' => 'circle-o', 'url' => ['user/index'],],
                                    ['label' => 'User Profile', 'icon' => 'circle-o', 'url' => ['user-profile/index'],],
                        ],
                    ],
                    [
                        'label' => 'Finance',
                        'icon' => 'line-chart',
                        'url' => '#',
                        'items' => [
                                    ['label' => 'OPEX', 'icon' => 'circle-o', 'url' => ['opex/index'],],
                        ],
                    ],
                    [
                        'label' => 'Operational',
                        'icon' => 'hourglass-half',
                        'url' => '#',
                        'items' => [
                                    ['label' => 'Buiding', 'icon' => 'circle-o', 'url' => ['build/index'],],
                        ],
                    ],
                    [
                        'label' => 'Donation',
                        'icon' => 'dollar',
                        'url' => '#',
                        'items' => [
                                    ['label' => 'Online', 'icon' => 'circle-o', 'url' => ['donation-online/index'],],
                                    ['label' => 'Offlien', 'icon' => 'circle-o', 'url' => ['donation-offline/index'],],
                        ],
                    ],
                    ['label' => 'Agenda', 'icon' => 'calendar', 'url' => ['agenda/index']],
                    ['label' => 'Developer', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'bug', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>


    </section>

</aside>
