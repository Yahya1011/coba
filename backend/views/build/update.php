<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Build */

$this->title = 'Update Build: ' . $model->id_build;
$this->params['breadcrumbs'][] = ['label' => 'Builds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_build, 'url' => ['view', 'id' => $model->id_build]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="build-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
