<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Subdistrict

/* @var $this yii\web\View */
/* @var $model backend\models\Village */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="village-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subdistricts_id')->dropDownList(
    	ArrayHelper::map(Subdistrict::find()->all(), 'id_subdistrict','name'),
    	['prompt'=>'Select Subdistrict'] 
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
