<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Pic;
use backend\models\Event;

/* @var $this yii\web\View */
/* @var $model backend\models\Agenda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="agenda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pic_id_pic')->dropDownList(
    	ArrayHelper::map(Pic::find()->all(), 'pic_id_pic','name'),
    	['prompt'=>'Select Pic']
    ) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'event_id')->dropDownList(
    	ArrayHelper::map(Event::find()->all(), 'event_id', 'name'),
    	['prompt'=>'Select Event']
    ) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
