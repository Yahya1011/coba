<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DonationOffline */

$this->title = 'Create Donation Offline';
$this->params['breadcrumbs'][] = ['label' => 'Donation Offlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donation-offline-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
