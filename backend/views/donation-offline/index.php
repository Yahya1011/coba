<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DonationOfflineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Donation Offline';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donation-offline-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Donation Offline', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_donation_offline',
            'code_trx',
            'date',
            'cat_donation_id',
            'username',
            //'nominal',
            //'info',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
