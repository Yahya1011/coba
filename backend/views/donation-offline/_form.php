<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CatDonation;

/* @var $this yii\web\View */
/* @var $model backend\models\DonationOffline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donation-offline-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code_trx')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'cat_donation_id')->dropDownList(
        ArrayHelper::map(CatDonation::find()->all(), 'cat_donation_id', 'name'),
        ['prompt'=>'Select CatDonation']
    ) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nominal')->textInput() ?>

    <?= $form->field($model, 'info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
