<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DonationOffline */

$this->title = 'Update Donation Offline: ' . $model->id_donation_offline;
$this->params['breadcrumbs'][] = ['label' => 'Donation Offlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_donation_offline, 'url' => ['view', 'id' => $model->id_donation_offline]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="donation-offline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
