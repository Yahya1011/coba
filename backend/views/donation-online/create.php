<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DonationOnline */

$this->title = 'Create Donation Online';
$this->params['breadcrumbs'][] = ['label' => 'Donation Onlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donation-online-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
