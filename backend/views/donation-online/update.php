<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DonationOnline */

$this->title = 'Update Donation Online: ' . $model->id_donation_online;
$this->params['breadcrumbs'][] = ['label' => 'Donation Onlines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_donation_online, 'url' => ['view', 'id' => $model->id_donation_online]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="donation-online-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
