<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Bank;

/* @var $this yii\web\View */
/* @var $model backend\models\TrxBank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-bank-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code_trx')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_id')->dropDownList(
        ArrayHelper::map(Bank::find()->all(), 'bank_id', 'name'),
        ['prompt'=>'Select Bank']
    ) ?>

    <?= $form->field($model, 'no_rek_destination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_name_destination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nominal')->textInput() ?>

    <?= $form->field($model, 'info')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_trx')->dropDownList([ 'Tunai' => 'Tunai', 'Transfer' => 'Transfer', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'no_machine')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Berhasil' => 'Berhasil', 'Gagal' => 'Gagal', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
