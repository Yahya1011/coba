<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TrxBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trx Bank';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-bank-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trx Bank', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_trx_bank',
            'code_trx',
            'bank_id',
            'no_rek_destination',
            'account_name_destination',
            //'nominal',
            //'info',
            //'type_trx',
            //'no_machine',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
