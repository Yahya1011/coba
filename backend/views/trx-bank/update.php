<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrxBank */

$this->title = 'Update Trx Bank: ' . $model->id_trx_bank;
$this->params['breadcrumbs'][] = ['label' => 'Trx Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_trx_bank, 'url' => ['view', 'id' => $model->id_trx_bank]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trx-bank-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
