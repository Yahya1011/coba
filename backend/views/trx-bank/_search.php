<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TrxBankSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trx-bank-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_trx_bank') ?>

    <?= $form->field($model, 'code_trx') ?>

    <?= $form->field($model, 'bank_id') ?>

    <?= $form->field($model, 'no_rek_destination') ?>

    <?= $form->field($model, 'account_name_destination') ?>

    <?php // echo $form->field($model, 'nominal') ?>

    <?php // echo $form->field($model, 'info') ?>

    <?php // echo $form->field($model, 'type_trx') ?>

    <?php // echo $form->field($model, 'no_machine') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
