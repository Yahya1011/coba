<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TrxBank */

$this->title = $model->id_trx_bank;
$this->params['breadcrumbs'][] = ['label' => 'Trx Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trx-bank-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_trx_bank], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_trx_bank], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_trx_bank',
            'code_trx',
            'bank_id',
            'no_rek_destination',
            'account_name_destination',
            'nominal',
            'info',
            'type_trx',
            'no_machine',
            'status',
        ],
    ]) ?>

</div>
