<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Opex */

$this->title = 'Create Opex';
$this->params['breadcrumbs'][] = ['label' => 'Opexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opex-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
