<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CatCoa;
use backend\models\SubcatCoa;
use backend\models\Pic;

/* @var $this yii\web\View */
/* @var $model backend\models\Opex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opex-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code_trx')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'cat_coa_id')->dropDownList(
        ArrayHelper::map(CatCoa::find()->all(), 'cat_coa_id', 'name'),
        ['prompt'=>'Select Cat Coa']
    ) ?>

    <?= $form->field($model, 'subcat_coa_id')->dropDownList(
        ArrayHelper::map(SubcatCoa::find()->all(), 'subcat_coa_id', 'name'),
        ['prompt'=>'Select Subcat Coa']
    ) ?>

    <?= $form->field($model, 'nominal')->textInput() ?>

    <?= $form->field($model, 'pic_id')->dropDownList(
        ArrayHelper::map(Pic::find()->all(), 'pic_id', 'name'),
        ['prompt'=>'Select Pic']
    ) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
