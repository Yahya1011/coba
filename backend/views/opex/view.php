<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Opex */

$this->title = $model->id_opex;
$this->params['breadcrumbs'][] = ['label' => 'Opexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opex-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_opex], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_opex], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_opex',
            'code_trx',
            'date',
            'cat_coa_id',
            'subcat_coa_id',
            'nominal',
            'pic_id',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
