<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Opex */

$this->title = 'Update Opex: ' . $model->id_opex;
$this->params['breadcrumbs'][] = ['label' => 'Opexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_opex, 'url' => ['view', 'id' => $model->id_opex]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="opex-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
