<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CatDonation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cat-donation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code_donation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
