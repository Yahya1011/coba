<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\CatDonation */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cat Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cat-donation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_cat_donation], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_cat_donation], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_cat_donation',
            'code_donation',
            'name',
        ],
    ]) ?>

</div>
