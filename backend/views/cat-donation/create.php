<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CatDonation */

$this->title = 'Create Cat Donation';
$this->params['breadcrumbs'][] = ['label' => 'Cat Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cat-donation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
