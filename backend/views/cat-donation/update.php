<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CatDonation */

$this->title = 'Update Cat Donation: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cat Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_cat_donation]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cat-donation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
