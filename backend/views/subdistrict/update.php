<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Subdistrict */

$this->title = 'Update Subdistrict: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subdistricts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_subdistrict]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subdistrict-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
