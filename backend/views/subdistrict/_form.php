<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\CityDistrict;

/* @var $this yii\web\View */
/* @var $model backend\models\Subdistrict */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subdistrict-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_district_id')->dropDownList(
    	ArrayHelper::map(CityDistrict::find()->all(), 'id_city_district','name'),
    	['prompt'=>'select city']
    ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
