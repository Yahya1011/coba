<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CatBuild */

$this->title = 'Update Cat Build: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cat Builds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_cat_build]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cat-build-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
