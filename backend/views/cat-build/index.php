<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CatBuildSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cat Build';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cat-build-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cat Build', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_cat_build',
            'name',
            'bobot',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
