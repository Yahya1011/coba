<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CatBuild */

$this->title = 'Create Cat Build';
$this->params['breadcrumbs'][] = ['label' => 'Cat Builds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cat-build-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
