<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CatCoa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cat-coa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code_coa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
