<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CatCoa */

$this->title = 'Create Cat Coa';
$this->params['breadcrumbs'][] = ['label' => 'Cat Coas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cat-coa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
